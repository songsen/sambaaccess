#ifndef SAMBACOPY_H
#define SAMBACOPY_H

#include <QString>
#include <QDateTime>
#include <QString>

class sambaCopy
{
public:
    sambaCopy();
    sambaCopy(QString link);
    ~sambaCopy();

    void setFromDir(QString &link);
    void setToDir(QString &path);

    //拷贝指定日期的文件
    bool copyFilesByTime(QDateTime from,QDateTime to) const;
    bool copyFile();
    bool copyAll();

    //通用拷贝函数
    static bool copyFile(QString &srcFile ,QString &toDir, bool coverFileIfExist);
    static bool copyAll(const QString &fromDir, const QString &toDir, bool coverFileIfExist);

 private:
    QString srcPath;
    QString desPath;
    QString srcFile;

};

#endif // SAMBACOPY_H
