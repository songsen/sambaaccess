#include "dialog.h"
#include "ui_dialog.h"
#include <QFileDialog>
#include <QtDebug>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    remotePath = "//10.2.3.2";
    sambaClient = new sambaCopy;
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButton_srcPath_clicked()
{
    srcPath = QFileDialog::getOpenFileName(this,tr("open file"),remotePath);
    qDebug()<<srcPath;
    sambaClient->setFromDir(srcPath);
}



void Dialog::on_pushButton_savePath_clicked()
{
    desPath = QFileDialog::getExistingDirectory(this,tr("save file"),QDir::currentPath());
    sambaClient->setToDir(desPath);
}

void Dialog::on_pushButton_copy_clicked()
{
    sambaClient->copyFilesByTime(ui->dateTimeEdit_from->dateTime(),ui->dateTimeEdit_to->dateTime());
}

void Dialog::on_pushButton_copyfile_clicked()
{
    sambaClient->copyFile();
}

void Dialog::on_pushButton_copydir_clicked()
{
    sambaClient->copyAll();
}
