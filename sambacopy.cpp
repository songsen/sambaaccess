#include "sambacopy.h"
#include <QDir>
//#include <QDebug>

sambaCopy::sambaCopy()
{

}
sambaCopy::sambaCopy(QString link)
{
    setFromDir(link);
}
sambaCopy::~sambaCopy()
{

}

void sambaCopy::setFromDir(QString &link)
{
    if(link.size() == 0){
        return ;
    }
    QFileInfo info(link);
    if(info.isFile()){
        srcFile = link;
        srcPath = info.path();
    }else{
        srcPath = link;
    }
}

void sambaCopy::setToDir(QString &path)
{
    if(path.size() == 0){
        return ;
    }

    QFileInfo info(path);
    if(info.isFile()){
        desPath = info.path();
    }else{
        desPath = path;
    }

}

bool sambaCopy::copyFilesByTime(QDateTime from,QDateTime to) const
{

   if(desPath.size() == 0){
       return  false;
   }

   if(srcPath.size() == 0){
       return  false;
   }

   QDir sourceDir(srcPath);
   QDir targetDir(desPath);

   if(!targetDir.exists()){
       if(!targetDir.mkdir(targetDir.absolutePath()))
           return false;
   }

   QFileInfoList fileInfoList = sourceDir.entryInfoList();
   foreach(QFileInfo fileInfo, fileInfoList){
       if(fileInfo.fileName() == "." || fileInfo.fileName() == "..")
           continue;

       if(fileInfo.isDir()){
           continue;
       }

       if((fileInfo.lastModified()>from)&&(fileInfo.lastModified()<to)){
           /// 进行文件copy
           if(!QFile::copy(fileInfo.filePath(),
               targetDir.filePath(fileInfo.fileName()))){
                   return false;
           }
       }
    }

   return true;
}

bool sambaCopy::copyFile()
{
   return copyFile(srcFile,desPath,true);
}

bool sambaCopy::copyAll()
{
    return copyAll(srcPath,desPath,true);
}

//拷贝文件：
bool sambaCopy::copyFile(QString &srcFile ,QString &toDir, bool coverFileIfExist)
{
    if(srcFile.size() == 0){
        return  false;
    }

    if(toDir.size() == 0){
        return  false;
    }

    toDir.replace("\\","/");
    if (srcFile == toDir){
        return true;
    }
    if (!QFile::exists(srcFile)){
        return false;
    }
    QDir *createfile     = new QDir;
    bool exist = createfile->exists(toDir);
    if (exist){
        if(coverFileIfExist){
            createfile->remove(toDir);
        }
    }//end if

    QFileInfo fileInfo(srcFile);
    QDir targetDir(toDir);

    //qDebug()<<srcFile<< "-- TO -->" << targetDir.filePath(fileInfo.fileName());
    if(!QFile::copy(srcFile, targetDir.filePath(fileInfo.fileName())))
    {
        return false;
    }
    return true;
}

//拷贝文件夹：
bool sambaCopy::copyAll(const QString &fromDIR, const QString &toDir, bool coverFileIfExist)
{
    if(fromDIR.size() == 0){
        return  false;
    }

    if(toDir.size() == 0){
        return  false;
    }

    QDir sourceDir(fromDIR);
    QDir targetDir(toDir);
    if(!targetDir.exists()){    /**< 如果目标目录不存在，则进行创建 */
        if(!targetDir.mkdir(targetDir.absolutePath()))
            return false;
    }

    QFileInfoList fileInfoList = sourceDir.entryInfoList();
    foreach(QFileInfo fileInfo, fileInfoList){
        if(fileInfo.fileName() == "." || fileInfo.fileName() == "..")
            continue;

        if(fileInfo.isDir()){    /**< 当为目录时，递归的进行copy */
            if(!copyAll(fileInfo.filePath(),
                targetDir.filePath(fileInfo.fileName()),
                coverFileIfExist))
                return false;
        }
        else{                   /**< 当允许覆盖操作时，将旧文件进行删除操作 */
            if(coverFileIfExist && targetDir.exists(fileInfo.fileName())){
                targetDir.remove(fileInfo.fileName());
            }

            /// 进行文件copy
            if(!QFile::copy(fileInfo.filePath(),
                targetDir.filePath(fileInfo.fileName()))){
                    return false;
            }
        }
    }
    return true;
}
