#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include "sambacopy.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();

private slots:
    void on_pushButton_srcPath_clicked();

    void on_pushButton_savePath_clicked();

    void on_pushButton_copy_clicked();

    void on_pushButton_copyfile_clicked();

    void on_pushButton_copydir_clicked();

private:
    Ui::Dialog *ui;
    QString srcPath;
    QString desPath;
    QString remotePath;
    sambaCopy *sambaClient;
};

#endif // DIALOG_H
